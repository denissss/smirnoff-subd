/***************************************************************
 * Name:      kfaMain.h
 * Purpose:   Defines Application Frame
 * Author:    denissff (denis0smirnov@gmail.com)
 * Created:   2019-03-23
 * Copyright: denissff (drugs.com)
 * License:
 **************************************************************/

#ifndef KFAMAIN_H
#define KFAMAIN_H

//(*Headers(kfaFrame)
#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
//*)

class kfaFrame: public wxFrame
{
    public:

        kfaFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~kfaFrame();

    private:

        //(*Handlers(kfaFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        //*)

        //(*Identifiers(kfaFrame)
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(kfaFrame)
        wxStatusBar* StatusBar1;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // KFAMAIN_H

/***************************************************************
 * Name:      kfaApp.h
 * Purpose:   Defines Application Class
 * Author:    denissff (denis0smirnov@gmail.com)
 * Created:   2019-03-23
 * Copyright: denissff (drugs.com)
 * License:
 **************************************************************/

#ifndef KFAAPP_H
#define KFAAPP_H

#include <wx/app.h>

class kfaApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // KFAAPP_H

/***************************************************************
 * Name:      kfaApp.cpp
 * Purpose:   Code for Application Class
 * Author:    denissff (denis0smirnov@gmail.com)
 * Created:   2019-03-23
 * Copyright: denissff (drugs.com)
 * License:
 **************************************************************/

#include "kfaApp.h"

//(*AppHeaders
#include "kfaMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(kfaApp);

bool kfaApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	kfaFrame* Frame = new kfaFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
